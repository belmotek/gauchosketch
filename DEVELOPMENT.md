## Drawing
A drawing is, basically, a collection of graphical elements, metadata and variables.

## Neutral
Neutral is a neutral CAD format developed by Belmotek for its software GauchoSketch is a data structure that allow to storage a drawing or exchange information between diferents software.

### Elements
Elements are the graphical elements such lines, arcs ellipses etc. All the elements have a 8 core parameters described below:

| symbol  |  Description                      | Type    |
|---------|-----------------------------------|---------|
| type    | Element type [1]                  | Byte    |
| index   | Element index                     | String  |
| set     | Model or Layaouts [2]             | Byte    |
| level   | Level (Layer)                     | Byte    |
| style   | Style (Line type)                 | Byte    |
| color   | Color                             | Byte    |
| weight  | Weight                            | Byte    |
| meta    | Metadata [0]                      | String  |
| state   | Normal, construction, phantom [9] | String  |


 - 0) Metadata is a extra field to storage data such link, BIM information, material code, etc.
 - 1) See ISFF Elements table.
 - 2) 0 = Model Space, 1 = Layout 1, n = Llayout n.
 - 3) Depend of drawing dimensions (2 or 3) the numbers of floats to define a coordinate will be 2 or 3.
 - 4) Is the polygon is hole value is 0.
 - 5) Rotaion in 2d is indicated as Float its mena the angle in radians, in 3d rotation is indicated as a quaternion.
 - 6) Angle in radians.
 - 7) Font is a number.
 - 8) Complex element contains a list of the indices of the member elements.
 - 9) States of element are normal (1), construction (2) or panthom (3).
 
Then, depend of type of element, there are more parameters

#### Line

|  symbol  |  Description   | Type    |
|----------|----------------|---------|
| p        | Points     [3] | FLoat[] |


#### Polyline

|  symbol   |  Description   | Type    |
|-----------|----------------|---------|
| p         | Points     [3] | FLoat[] |
| verticces | Vertices       | Integer |

#### Curve

|  symbol   |  Description    | Type    |
|-----------|-----------------|---------|
| p         | Points     [3]  | FLoat[] |
| verticces | Vertices       | Integer |

#### Polyline

|  symbol   |  Description   | Type    |
|-----------|----------------|---------|
| p         | Points     [3] | FLoat[] |
| verticces | Vertices       | Integer |

#### Polygon

|  symbol   |  Description   | Type    |
|-----------|----------------|---------|
| p         | Points     [3] | FLoat[] |
| verticces | Vertices       | Integer |
| pattern   | Pattern    [4] | Byte    |


#### Ellipse

|  symbol   |  Description          | Type     |
|-----------|-----------------------|----------|
| insert    | Points         [3]    | FLoat[]  |
| haxis     | Horizontal axis       | FLoat    |
| vaxis     | Vertical axis         | Float    |
| rotation  | 2D Rotation angle [5] | Float    |
| rotation  | 3D Rotation angle [5] | Float[4] |

#### Arc

|  symbol   |  Description          | Type     |
|-----------|-----------------------|----------|
| p         | Points         [3]    | FLoat[]  |
| haxis     | Horizontal axis       | FLoat    |
| vaxis     | Vertical axis         | Float    |
| rotation  | 2D Rotation angle [5] | Float    |
| rotation  | 3D Rotation angle [5] | Float[4] |
| angle1    | Start angle       [6] | Float    |
| angle2    | End angle         [6] | Float    |


#### Text

|  symbol        |  Description          | Type     |
|----------------|-----------------------|----------|
| p              | Points         [3]    | FLoat[]  |
| rotation       | 2D Rotation angle [5] | Float    |
| rotation       | 3D Rotation angle [5] | Float[4] |
| vsize          | Text height           | Float    |
| hsize          | Text width factor     | Float    |
| typeface       | Text font         [7] | Byte     |
| position       | Justification         | Byte     |
| tagname        | Text name  (tag)      | String   |
| prompt         | Text label (prompt)   | String   |
| value          | Text value            | String   |


#### Complex
Complex is a collection of elements like a block or cell

|  symbol   |  Description          | Type     |
|-----------|-----------------------|----------|
| p         | Points         [3]    | FLoat[]  |
| rotation  | 2D Rotation angle [5] | Float    |
| rotation  | 3D Rotation angle [5] | Float[4] |
| scale     | Complex scale     [3] | Float[]  |
| name      | Text name  (tag)      | String   |
| parts     | Components        [8] | String[] |

#### Text justifications

|  Justification   | Number |
|------------------|--------|
| Left Top         | 0      |
| Left Center      | 1      |
| Left Bottom      | 2      |
| Left Descender   | 3      |
| Center Top       | 4      |
| Center Center    | 5      |
| Center Bottom    | 6      |
| Center Descender | 7      |
| Right Top        | 8      |
| Right Center     | 9      |
| Right Bottom     | 10     |
| Right Descender  | 11     |


### Levels
Levels (layers) form 0 to 63 

File
Timestamp
Creation
Version

## Objects
The objects are Layer, Linetype, etc.

### Blocks
|  Tag      | Description | Type    |
|-----------|-------------|---------|
| name      |             | string  |
| handle    |             | String  |
| desc      | Description | String  |
| point_1   | Base point  | Float[] |
| anonymous |             |         |
| xref      |             |         |
| owner     |             |         |
| type      |             | Integer |

## ISFF Elements
|Element Number | Element Type|
| -- | -- |
|1 | Cell Library Header|
|2 | Cell Complex Header|
|3 | Line|
|4 | Line String|
|5 | Group Data|
|6 | Shape|
|7 | Text Node Header|
|8 | Digitizer Setup Data|
|9 | Design File Header|
|10 | Level Symbology|
|11 | Curve|
|12 | Complex Chain Header|
|13 | Conic|
|14 | Complex Shape Header|
|15 | Ellipse|
|16 | Arc|
|17 | Text|
|18 | 3D Surface Header|
|19 | 3D Solid Header|
|20 | Subfigure|
|21 | B-Spline Pole|
|22 | Point String|
|23 | 3D Circular Truncated Cone|
|24 | 3D B-Spline Surface Header|
|25 | 3D B-Spline Surface Boundary|
|26 | B-Spline Knot|
|27 | B-Spline Curve Header|
|28 | B-Spline Weight Factor|
|29-32 | Reserved|
|33 | Dimension|
|34 | Shared Cell Definition|
|35 | Shared Cell Instance|
|36 | Multi-Line|
|37 | Tag Data|
|38 | Smart Geometry|
|39-50 | Reserved|
|51 | ARCH|
|54 | SDS|
|56 | IEDS, WMS|
|57 | ICS, SDI (stereo plotter parameters)|
|58 | Text ARRAY|
|59 | Text STACK|
|60 | SIS|
|61 | EWS SCHEMATIC|
|62 | EWS PANEL|
|63 | EWS, PDS|
|64 | Application Element|
|65 | Application Element|
|66 | MicroStation Application Elements|
|67-86 | Application Element|
|87 | Raster Header (imported)|
|88 | Raster Data|
|89 | Application Element|
|90 | Binary Raster Header (referenced)|
|91 | Raster Reference Component|
|92 | Raster Hierarchy Element|
|93 | Raster Hierarchy Component|
|94 | Reserved for raster data|
|95 | Table Entry Element|33
|96 | Table Header Element|
|97 | View Group Element|
|98 | View Element|
|99 | Level Mask Element|
|100 | Reference Attach Element|
|101 | Matrix Header|
|102 | Matrix Int Data|
|103 | Matrix Double Data|
|105 | Mesh Header|
|108 | Reference Override Element|
|110 | Named Group Header|
|111 | Named Group Component|
|112-125 | Application Element|
|126 | User Defined|
|127 | Reserved|

## Entities
The entities are the objects like line circle etc.

|    Entity   |  dwg      |
|-------------|-----------|
|  arc        |           |
|  attdef     |           |
|  attrib     | attrib    |
|  block      |           |
|  circle     | circle    |
|  ellipse    |           |
|  insert     | insert    |
|  leader     |           |
|  line       | line      |
|             |           |
|  lwpolyline | lwpolyline|
|  mtext      |           |
|  pattern    |           |
|  point      |           |
|  polygon    |           |
|  ray        |           |
|  rectangle  |           |
|  solid      |           |
|  spline     |           |
|  text       |    text   |

### Basic structure for entities

|  Key       |  Description                                           |  Type     |
|------------|--------------------------------------------------------|-----------|
| object     |  Object type like line, circle etc.                    |  String   |
| type       |                                                        |  Integer  |
| handle     |  Handle of the object                                  |  String   |
| space      |  Object parent, model 0 or sketch 2,3...n, or Block 255|  Byte     |
| layer      |  Object layer                                          |  String   |
| ltype      |  Stroke line type                                      |  Byte     |
| width      |  Stroke width                                          |  Byte     |
| color      |  Stroke colour 0-255                                   |  Byte     |
| status     |  Current logistic of the object                        |  Byte     |
| invisible  |  Visibility of the object                              |  Integer  |
| selected   |  Object is selected or not                             |  Boolean  |
| prompt     |  Message when the object is created or altered         |  String   |

Notes: 
1) If space is 0 Gambas delete the item from collection, jsonCollection remains.

### Line

|  Key    |  Description|  Type   |
|---------|-------------|---------|
| point_1 |  stsart     |  Float[]|
| point_2 |  end        |  Float[]|
|         |             |         |

### Circle

|  Key    |  Description|  Type    |
|---------|-------------|----------|
| point_1 |  center     |  Float[] |
| radius  |  radious    |  Float   |

### Arc

|  Key    |  Description    |  Type    |
|---------|-----------------|----------|
| point_1 |  Circle center  |  Float[] |
| radius  |  Circle radious |  Float   |
| angle_1 |  start angle    |  Float   | 
| angle_2 |  end angle      |  Float   |

### Text, Attrib

|  Key     |  Description    |  Type   |
|----------|-----------------|-------- |
| text     |  text_value     |  String |
| tag      |  tag            |  String | 
| point_1  |  ins_pt         |  Float[]|
| height   |  height         |  Float  |
| point_2  |  alignment_pt   |  Float[]|
| w_factor |                 |  Float  |
| rotation |                 |  Float  |
| position |  horiz_alignment|  Byte   |
|          |                 |         |

### Inserts

|  Key    | Description                 | Type    |
|---------|-----------------------------|---------|
| point_1 | insertion point             | Float[] |
| scale   | scale                       | Float   |
| block   | block name                  | String  |
| angle_1 | Rotation angle              | Float   |
| atts    | has_attribs 1=True, 0=False | Byte    |
|         |                             |         |


## Import files 
There are some formats that Sketsch could import like dwg, dgn, svg, but here is a basic difference between them some of them are binari files others text files.
For import binary files Sketch use libraries to read the info contained in the file, ones the info is avalaible in text format ist is arranged to compliance whit the standard data collection.

### DGN v7 
The library dgnlib
http://dgnlib.maptools.org/

Line
Line String
Shape
Ellipse
Arc
Text
Cell Header
Group Data
Digitizer Setup
TCB
Level Symbology
Complex Shape Header
B-Spline Pole
B-Spline Knot
B-Spline Curve Header
B-Spline Weight Factor
Application Element


## Orders

### Basic structure for orders

|  Key       |  Description                                           |  Type     |
|------------|--------------------------------------------------------|-----------|
| object     |  Order type like Zoom extents, Delete, Undo, etc.      |  String   |
| prompt     |  Message when the order is activated or altered        |  String   |

### Zoom window

|  Key     |  Description         |    Type  |
|----------|----------------------|----------|
| point_1  |  Lower window Corner |  Float[] |
| point_2  |  Upper window Corner |  Float[] |
|          |                      |          |

## Print

### GNU Plot utils

#### 10 op codes for primitive graphics operations|as in Unix plot(5) format.

| Operation      | Symbol  | Remarks                        |
|----------------|---------|--------------------------------|
|  O_ARC         |  'a'    |                                |
|  O_CIRCLE      |  'c'    |                                |
|  O_CONT        |  'n'    |                                |
|  O_ERASE       |  'e'    |                                |
|  O_LABEL       |  't'    |                                |
|  O_LINEMOD     |  'f'    |                                |
|  O_LINE        |  'l'    |                                |
|  O_MOVE        |  'm'    |                                |
|  O_POINT       |  'p'    |                                |
|  O_SPACE       |  's'    |                                |

#### 42 op codes that are GNU extensions

| Operation      | Symbol  | Remarks                        |
|----------------|---------|--------------------------------|
|  O_ALABEL      |  'T'    |                                |
|  O_ARCREL      |  'A'    |                                |
|  O_BEZIER2     |  'q'    |                                |
|  O_BEZIER2REL  |  'r'    |                                |
|  O_BEZIER3     |  'y'    |                                |
|  O_BEZIER3REL  |  'z'    |                                |
|  O_BGCOLOR     |  '~'    |                                |
|  O_BOX         |  'B'    | not an op code in Unix plot(5) |
|  O_BOXREL      |  'H'    |                                |
|  O_CAPMOD      |  'K'    |                                |
|  O_CIRCLEREL   |  'G'    |                                |
|  O_CLOSEPATH   |  'k'    |                                |
|  O_CLOSEPL     |  'x'    | not an op code in Unix plot(5) |
|  O_COMMENT     |  '#'    |                                |
|  O_CONTREL     |  'N'    |                                |
|  O_ELLARC      |  '?'    |                                |
|  O_ELLARCREL   |  '/'    |                                |
|  O_ELLIPSE     |  '+'    |                                |
|  O_ELLIPSEREL  |  '|'    |                                |
|  O_ENDPATH     |  'E'    |                                |
|  O_ENDSUBPATH  |  ']'    |                                |
|  O_FILLTYPE    |  'L'    |                                |
|  O_FILLCOLOR   |  'D'    |                                |
|  O_FILLMOD     |  'g'    |                                |
|  O_FONTNAME    |  'F'    |                                |
|  O_FONTSIZE    |  'S'    |                                |
|  O_JOINMOD     |  'J'    |                                |
|  O_LINEDASH    |  'd'    |                                |
|  O_LINEREL     |  'I'    |                                |
|  O_LINEWIDTH   |  'W'    |                                |
|  O_MARKER      |  'Y'    |                                |
|  O_MARKERREL   |  'Z'    |                                |
|  O_MOVEREL     |  'M'    |                                |
|  O_OPENPL      |  'o'    | not an op code in Unix plot(5) |
|  O_ORIENTATION |  'b'    |                                |
|  O_PENCOLOR    |  '-'    |                                |
|  O_PENTYPE     |  'h'    |                                |
|  O_POINTREL    |  'P'    |                                |
|  O_RESTORESTATE|  'O'    |                                |
|  O_SAVESTATE   |  'U'    |                                |
|  O_SPACE2      |  ':'    |                                |
|  O_TEXTANGLE   |  'R'    |                                |

#### 30 floating point counterparts to many of the above.  They are not even slightly mnemonic.

| Operation      | Symbol  | Remarks                        |
|----------------|---------|--------------------------------|
|  O_FARC        |  '1'    |                                |
|  O_FARCREL     |  '2'    |                                |
|  O_FBEZIER2    |  '`'    |                                |
|  O_FBEZIER2REL |  '\''   |                                |
|  O_FBEZIER3    |  '|'    |                                |
|  O_FBEZIER3REL |  '.'    |                                |
|  O_FBOX        |  '3'    |                                |
|  O_FBOXREL     |  '4'    |                                |
|  O_FCIRCLE     |  '5'    |                                |
|  O_FCIRCLEREL  |  '6'    |                                |
|  O_FCONT       |  ')'    |                                |
|  O_FCONTREL    |  '_'    |                                |
|  O_FELLARC     |  '}'    |                                |
|  O_FELLARCREL  |  '|'    |                                |
|  O_FELLIPSE    |  '{'    |                                |
|  O_FELLIPSEREL |  '['    |                                |
|  O_FFONTSIZE   |  '7'    |                                |
|  O_FLINE       |  '8'    |                                |
|  O_FLINEDASH   |  'w'    |                                |
|  O_FLINEREL    |  '9'    |                                |
|  O_FLINEWIDTH  |  '0'    |                                |
|  O_FMARKER     |  '!'    |                                |
|  O_FMARKERREL  |  '@'    |                                |
|  O_FMOVE       |  '$'    |                                |
|  O_FMOVEREL    |  '%'    |                                |
|  O_FPOINT      |  '^'    |                                |
|  O_FPOINTREL   |  '&'    |                                |
|  O_FSPACE      |  '*'    |                                |
|  O_FSPACE2     |  ';'    |                                |
|  O_FTEXTANGLE  |  '('    |                                |

#### 3 op codes for floating point operations with no integer counterpart */

| Operation      | Symbol  | Remarks                        |
|----------------|---------|--------------------------------|
|  O_FCONCAT     |  '\\'   |                                |
|  O_FMITERLIMIT |  'i'    |                                |
|  O_FSETMATRIX  |  'j'    |                                |

O_JOINMOD	=	'J',
Recognized styles are "miter" (the default), "round", and "bevel".

## HPGL/2 Language

Examples of HP-GL/2 commands
Command	Meaning
NPx	number of pens; x=1..256
PCx,r,g,b	pen color; x=pen, r=red, g=green, b=blue, 0..255
PWw,x	pen width; w=pen width in mm with decimal point, x=pen


## Mehods and functions

### Command

### Reorganize
"drawing", "blocks", "properties", "colors", "data"

## Load a SVG picture for buttons or others controls
```
     sPic = "./svg/file.svg"
    If Exist(sPic) Then
      svg = dsk.Contrary(sPic, "#0066b30", -1)
      pic = Image.FromString(svg).Stretch(32, 32).Picture
   Else
      pic = Stock["32/error"]
   Endif
```

## Formulas

### Como saber si un punto esta dentro, fuera o pertenece a una circunferencia
La distancia entre ⟨xc,yc⟩ y ⟨xp,yp⟩ viene dada por el teorema de Pitágoras como d=√(xp−xc)2+(yp−yc)2.
El punto ⟨xp,yp⟩ está dentro del círculo si d<r en el círculo si d=r y fuera del círculo si d>r.
Puede ahorrarse un poco de trabajo comparando d2 con r2 en cambio: el punto está dentro del círculo si d2<r2 en el círculo si d2=r2 y fuera del círculo si d2>r2.
Así, se quiere comparar el número (xp−xc)2+(yp−yc)2 con r2.
