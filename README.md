[![License](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://gitlab.com/belmotek/gauchosketch/LICENSE)

# GauchoSketch
GauchoSketch is a CAD program focused on engineering diagrams, such as wiring diagrams, flow diagrams, piping and instrument diagrams, logic diagrams etc.
It is currently under development, and we hope that it will soon be in alpha version to be able to use it.
Some concepts are taken from the RcadGB3 program, also made with Gambas. 

# Features
The aim is to be able to manage all the entities and their properties to create high quality graphical diagrams that can be printed on paper and published electronically.
It is also expected that it will be possible to access the information contained in the native file, which complies with the JSON standard, in order to be able to integrate it into production systems in engineering, manufacturing and construction projects.
Some aspects are simplified compared to other general-purpose CAD. For example there are only 16 styles, 32 thicknesses, 64 layers and 256 colours; all assigned with a number.

## Interface

- Clean interface with icons designed by us.
- Multiple documents at the same time
- Layer managment (64)
- Colour managment (256)
- Entities buttons
- File buttons
- Locks buttons also by function keys
- Mouse cursor custom
- Graphic and data modes
- Treeview to show all sketch data
- Keyboard commands, also by alias

## Entities

- Line
- Circle
- Polyline
- Rectangle
- Text

## Files
- Own really simple file format based on JSON standard.

    
>     {
    "Entity": "line",
    "Index": 1,
    "Block": false,
    "Level": 1,
    "Color": 1,
    "Style": 1,
    "Weight": 2,
    "Points": [[304, 89], [892, 307]]
    }
    

# TO DO

## Tools
- Move
- Copy
- Mirror
- Rotate
- Scale
- Delete

## Entities

- Ellipse
- Polygon

## Interface

- Improve <u>open</u>, <u>save</u>, <u>save as</u>, <u>select file</u>, warning if close without save, etc.
- Levels, add hide / show, color, style and print.
- <u>Improve enter commands</u> Acctually works for line and circle
- Show the current Line style
- Show the current Line weight
- <u>About section</u>
- Help section
- Automatic saving
- Add Patterns panel
- Program configuration management
- File variable management
- <u>View Scale</u>
- View panning
- Printing form
- Block Library Management

## Export as images

- Export as <u>PDF</u>
- Export as TIFF
- Export as PNG
- Export as GIF
- Export as JPEG

## Export as other CAD formats

- Export DWG Files ?
- Export DXF Files
- Export <u>SVG</u> Files
- Export IGES files
- Export G-CODE files

## Files import

- Import <u>DWG</u> files
- Import DGN files
- Import DXF Files
- Import SVG Files
- Import IGES files
- Import G-CODE files

## Screen shots
![Main window](gauchosketch_screenshoot_1.png)

![Levels](gauchosketch_screenshoot_2.png)

![Colors](gauchosketch_screenshoot_3.png)

![Styles](gauchosketch_screenshoot_4.png)

## Features
- Read dwg files and show its information y a friendly data interface called data explorer.

### DATA Explorer interface
Here it is possible to see the data of the drawing in table format. There is also a selection menu by type of entity. It is possible to see the basic data such as layer, line type, color, weight and also some other especific data related to the type of entity, for example in the text entity it is possible to see its value, in the lines and polylines its length etc.

# Entities

| Alias | Name | Description     |
|:-----:|:----:|---------------:|
|  l    | Line | Create a line |
| po    | Pline | Create a polyline |
| cv    | Circle | Create a circle |
|   a   | Arc | Create an arc |
|  ew   | Ellipse | Create an ellipse |
|   t   | Text | Create a text |
|  tt   | Mtext | Create a mtext |
|  at   | atribute | Create an atribute definition |
|  li   | Leader | Create a leader |
|  ii   | Insert | Insert a block insert |
|   b   | block | Create a block |
|  sq   | Spline | Create an spline |
|  sa   | Solid | Create an solid |
|  re   | Rectangle | Create an rectangle |
|  pl   | polygon | Create a polygon |
|  ht   | Hatch | Create an hatch |
|  pt   | point | Create a point |
|  ry   | Rectangle | Create an ray trace |

# Dimensions
| Alias | Name | Description |
|:-----:|:----:|------------:|
|   d0 | dimension_linear | Linear dimension |
|  d1 | dimension_aligned | Aligned dimension |
|  d2 | dimension_ang3pt | Angular dimension |
|  d3 | arc_dimension | Arc lenght dimension |
|  d4 | dimension_radius | Dimension radius |
|  d5 | dimension_diameter | Dimension diameter |
|  d6 | dimension_radius | Dimension large radial |
|  d7 | ordinate_dimension | Dimension ordinate |


# Tools
| Alias | Name | Description |
|:-----:|:----:|------------:|
|  m | Move | Move a site entity |
|  c | Copy | Copy an entity |
| sc | Scale | Scale an entity |
|  rt | Rotate | Rotate an entity |
|  mn | Mirror | Create a new entity mirroring another |
|  tr | Trim | Cut an entity |
|  f | Fillet | Create a tangential arc to two lines |
|  cd | Chamfer | Create a chamfer from two lines |
|  ar | Array | Create a patern, it could be rectangular or polar |
|  s | Stretch | Stretch an entity |
|  fg | Offset | Create a parallel entity |
|  dz | Divide | Divide an entity |
|  bg | Break | Brak a entity in two parts |
|  e | Extend | Stretch an entity from one end |
|  de | erase | Delete an entity |

# Inquiry
| Alias | Name | Description |
|:-----:|:----:|------------:|
|   mh | area | Meassure area |
|  mj | ruler | Measure distance |
|  mk | protractos | Measure angle |

# View
| Alias | Name | Description |
|:-----:|:----:|------------:|
|  zc | Pan | Displacement of sight |
|  zx | Zoome | View fit to all entities |
|  z | Zoomw | View window |

# Levels
| Alias | Name | Description |
|:-----:|:----:|------------:|
|  lk | Levels |  |

# Locks
| Key | Name | Description |
|:-----:|:----:|------------:|
|  F1 | Help |  |
|  F2 | Spare |  |
|  F3 | Object snap |  |
|  F4 | Level simbology |  |
|  F5 | Spare |  |
|  F6 | Bounds | Enable or disable the bounds in drawing |
|  F7 | Spare |  |
|  F8 | Ortho | Enable or disable the orthogonal mode |
|  F9 | Grid Snap | Enable or disable the grid |
|  F10 | Spare |  |
|  F11 | Spare |  |
|  F12 | Spare |  |

## Contact
info@belmotek.net

## LibreDWG  project
LibreDWG - free implementation of the DWG file format, is a free C library to read and write DWG files. This program is part of the GNU project, released under the aegis of GNU. It is licensed under the terms of the GNU General Public License version 3 (or at you option any later version).

[https://www.gnu.org/software/libredwg/](URL)

### Install
```bash
    #!/bin/bash
    # Script para instalar LibreDWG en el sistema
    #===============================================================================
    # LibDWG
    git clone git://git.sv.gnu.org/libredwg.git
    cd libredwg
    sh autogen.sh
    ./configure --enable-trace
    make
    sudo make install
    make check
    cd ..
```

## DGNLib Project
DGNLib: a Microstation DGN (ISFF) Reader
http://dgnlib.maptools.org/

```bash
    #!/bin/bash
    # Script para instalar dgnlib en el sistema
    #===============================================================================
    # dgnlib
    wget  http://dgnlib.maptools.org/dl/dgnlib-1.11.zip
    unzip dgnlib-1.11.zip
    cd dgnlib-1.11
    make
    sudo install dgndump /usr/local/bin/dgndump
    sudo install dgnwritetest /usr/local/bin/dgnwritetest
    cd ..
```


## Gambas project
Gambas is a free development environment and a full powerful development platform based on a Basic interpreter with object extensions.

[http://gambas.sourceforge.net/en/main.html](URL)